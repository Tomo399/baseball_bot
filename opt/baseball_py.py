#!/usr/bin/env python3

import logging

import redis
import requests
from bs4 import BeautifulSoup
from slackclient import SlackClient

URL = "https://baseball.yahoo.co.jp/npb/"
TOKEN = "xoxp-xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
CHANNEL = "XXXXXXXXX"
NAME = "baseball_bot"
ICON = ":baseball:"
REDIS_HOST = "127.0.0.1"

logger = logging.getLogger(__name__)
logger.addHandler(logging.StreamHandler())
logger.setLevel(logging.DEBUG)


def parse():
    r = requests.get(URL)
    soup = BeautifulSoup(r.text, "html.parser")

    results = []
    for table in soup.find_all("table", attrs={"class": "teams"}):
        teams = []
        for tr in table.find_all("tr"):
            if tr.find("th"):
                team = tr.th.a["title"]
                teams.append(team)
        trs = table.table.find_all("tr")
        scores = [
            trs[0].td.string,
            trs[2].td.string,
        ]
        result = trs[1].td.a.string
        url = trs[1].td.a["href"]
        res_diff = "{}: {} vs {}: {}".format(teams[0], scores[0], teams[1], scores[1])
        res = "{} (<{}|{}>)".format(res_diff, url, result)
        logger.debug(("parsed:", res))
        results.append([res, res_diff])
    return results


def notify(client, results):
    client.api_call(
        "chat.postMessage", channel=CHANNEL, text="\n".join(results),
        username=NAME, icon_emoji=ICON, link_names=True
    )


def main():
    results = parse()
    if not results:
        logger.debug("No results found")
        return
    r = redis.StrictRedis(host=REDIS_HOST, port=6379, db=10)
    c = SlackClient(TOKEN)
    diffs = []
    for idx, result in enumerate(results):
        key = "baseball:{}".format(idx)
        if r.get(key) != result[1].encode("utf-8"):
            r.set(key, result[1])
            diffs.append(result[0])
    if diffs:
        logger.debug(("diff:", diffs))
        notify(c, diffs)
    else:
        logger.debug("No diffs")


if __name__ == '__main__':
    main()