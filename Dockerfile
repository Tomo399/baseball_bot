FROM python:3
USER root

RUN apt-get update
RUN apt-get -y install locales && \
    localedef -f UTF-8 -i ja_JP ja_JP.UTF-8
ENV LANG ja_JP.UTF-8
ENV LANGUAGE ja_JP:ja
ENV LC_ALL ja_JP.UTF-8
ENV TZ JST-9
ENV TERM xterm

RUN apt-get install -y vim less redis-server cron
RUN pip install --upgrade pip
RUN pip install --upgrade setuptools
RUN pip install slackclient==1.3.2
RUN pip install redis
RUN pip install bs4

RUN echo '*/10 12-23 * * * /usr/local/bin/python3 /root/opt/baseball_py.py >/dev/null 2>&1' >> /etc/crontab
COPY run.sh /run.sh
RUN chmod 744 /run.sh
CMD ["/run.sh"]
